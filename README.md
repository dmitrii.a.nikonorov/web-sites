# :card_file_box: Материалы к вебинару 'Начните работу с Kubernetes: запускаем приложение'

:floppy_disk: В данном репозитории собраны материалы к вебинару,
организованному [Selectel](https://selectel.ru) и проведённому 10.08.2022.

:rocket: Для запуска нагрузки были использованы услуги [Managed Kubernetes](https://selectel.ru/services/cloud/kubernetes)
и [Container Registry](https://selectel.ru/services/cloud/container-registry)

:video_camera: Запись вебинара доступна на **Youtube**:

[![Начните работу с Kubernetes: запускаем приложение и отвечаем на все вопросы про K8s](https://img.youtube.com/vi/ILm5GeQiOhg/0.jpg)](https://youtu.be/ILm5GeQiOhg "Начните работу с Kubernetes: запускаем приложение и отвечаем на все вопросы про K8s")

:page_facing_up: Шаблоны для примеров взяты с [free-css.com](https://www.free-css.com):

- [Koppee](https://www.free-css.com/free-css-templates/page281/koppee)
- [Limelight](https://www.free-css.com/free-css-templates/page281/limelight)
- [Zay Shop](https://www.free-css.com/free-css-templates/page281/zay-shop)
